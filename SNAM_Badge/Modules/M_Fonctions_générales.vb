﻿Imports MadMilkman.Ini

Imports System.IO
Imports System.Numerics
Module M_Fonctions_générales

    'Friend Ds_csv As New DataSet

    Public Sub connect_to_database_ACT()
        Dim file As New IniFile()
        Dim pwdsa As String = ""
        Dim instance As String
        Dim catalog As String
        Dim user As String
        conn = New ACT.Datas.SQLite
        file.Load(DossierConfig & "\config.ini")


        Dim connectionString
        connectionString = "Data Source=" & file.Sections("ConnectionString").Keys("instance").Value & ";Version=3;"

        Try
                conn.OpenConnectionParChaine(connectionString)
            Catch ex As Exception
                Throw New Exception(ex.Message & vbCrLf & connectionString, ex)
        End Try

    End Sub

    Public Sub connect_to_database_SNAM()

        conn_SNAM = New ACT.Datas.SQLServer(ParamsSqlSnam("Serveur"), ParamsSqlSnam("Catalog"), ParamsSqlSnam("User"), MonCrypt.DechiffrerChaine(ParamsSqlSnam("Pwd")))

        Try
            conn_SNAM.OpenConnection()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

    End Sub



    Friend Sub updateBadge(ByVal Optional Test As Boolean = False)
        Try

            'Dim BorneFolder As Boolean = IO.Directory.Exists(ParamsBorne("ExportFolder"))
            Dim MonDs As New DataSet

            'If BorneFolder = True Then

            conn.AddSqlCommandToDataSet(MonDs, "BadgeAct", "Select ID_act,Snam_BigInt,replace(CAST(Badge_ACT AS TEXT),'_','') as  Badge_ACT,Snam_Hex,Cast(Last_Value AS TEXT) as Last_Value FROM Badges")
            If MonDs.Tables("BadgeAct").Rows.Count <> 0 Then
                Dim mesbadges As New List(Of String)


                For Each row In MonDs.Tables("BadgeAct").Rows
                    If row("SNAM_Hex").ToString.Trim <> "" Then
                        mesbadges.Add("CONVERT(BIGINT,0X" & row("SNAM_Hex") & ")")
                    End If
                Next


                Dim sqlstring = "select b.numero, p.nom, p.prenom, p.nationalite, p.matricule, p.commentaire, s.raison_sociale  ,ISNULL(p.nom,'') +'#'+ISNULL( p.prenom,'')+'#'+ISNULL(p.nationalite,'')+'#'+ISNULL( p.nationalite,'')+'#'+ISNULL( p.matricule,'') +ISNULL(  s.raison_sociale,'')  as [Checksum]
                                        FROM acbadge b 
                                        INNER Join acfiche_badge_profil_crise c On b.pkid = c.id_badge 
                                        INNER Join acfichepersonnelle p On c.id_fiche = p.pkid 
                                        INNER Join acsociete s On p.id_societe = s.pkid
                                        where b.numero in (" & Join(mesbadges.ToArray, ",") & ")"



                Dim paramactif As New Dictionary(Of String, String)
                Dim count = conn_SNAM.GetRowCount(sqlstring)

                If count > 0 Then


                    conn_SNAM.AddSqlCommandToDataSet(MonDs, "Badges_SNAM", sqlstring)


                    Form1.DataGridView1.DataSource = MonDs.Tables("Badges_SNAM")
                    MonDs.Relations.Add(MonDs.Tables("BadgeAct").Columns("SNAM_BigInt"), MonDs.Tables("Badges_SNAM").Columns("numero"))


                    MonDs.Tables("Badges_SNAM").Columns.Add("CheckSumACT", GetType(System.String), "Parent.Last_Value")

                    MonDs.Tables("Badges_SNAM").Columns.Add("ID_Badge", GetType(System.String), "Parent.Id_ACT")

                    MonDs.Tables("Badges_SNAM").Columns.Add("Code_Badge", GetType(System.String), "Parent.Badge_ACT")

                    MonDs.Tables("Badges_SNAM").Columns.Add("Code_SNAM", GetType(System.String), "Parent.Snam_hex")

                    Dim MesContenus As New Dictionary(Of Integer, String)
                    Dim MesAbonnes As New List(Of String)
                    For i = 0 To 5
                        If ParamsFichierValeur("Fichier" & i) <> "" Or ParamsFichierValeur("Fichier" & i) <> Nothing Then
                            paramactif.Add("Fichier_0" & i + 1, ParamsFichierValeur("Fichier" & i))

                        End If
                    Next
                    If MonDs.Tables("Badges_SNAM").Rows.Count > 0 Then
                        For Each row In MonDs.Tables("Badges_SNAM").Rows

                            'MsgBox(row("CheckSumACT").replace("#", "") & "<>" & row("Checksum").replace("#", ""))



                            Dim badge_decallage = row("ID_Badge") + 1
                            If row("CheckSumACT").replace("#", "") <> row("Checksum").replace("#", "") OrElse Test = True Then
                                For i = 0 To paramactif.Count - 1
                                    If MesContenus.Count < i + 1 Then
                                        MesContenus.Add(i, "")
                                    End If
                                    MesContenus(i) &= row("ID_Badge").ToString & vbTab & row(paramactif.Values(i)) & vbCrLf
                                Next
                                MesAbonnes.Add(badge_decallage.ToString & vbTab & "1" & vbTab & " " & vbTab & "#" & badge_decallage.ToString.PadLeft(3, "0"c) & badge_decallage.ToString.PadLeft(3, "0"c) & badge_decallage.ToString.PadLeft(3, "0"c) & badge_decallage.ToString.PadLeft(3, "0"c) & badge_decallage.ToString.PadLeft(3, "0"c) & badge_decallage.ToString.PadLeft(3, "0"c) & vbTab & "" & vbTab & "0" & vbTab & row("Code_Badge").ToString & vbTab & "0" & vbTab & "0" & vbTab & "000000" & vbTab & "1" & vbTab & "0" & vbTab & vbCrLf)

                            End If



                        Next
                    End If


                    Dim Sw As StreamWriter
                    If MesContenus.Count <> 0 Then
                        Dim I = 0
                        For Each param In paramactif
                            Sw = New StreamWriter(ParamsBorne("ExportFolder") & "\" & param.Key & ".txt")
                            Sw.Write(MesContenus(I))
                            Sw.Close()
                            Sw.Dispose()
                            I += 1
                        Next

                        Sw = New StreamWriter(ParamsBorne("ExportFolder") & "\abonnes.txt")
                        For i = 0 To MesAbonnes.Count - 1
                            Sw.Write(MesAbonnes(i))

                        Next
                        Sw.Close()
                        For Each row In MonDs.Tables("Badges_SNAM").Rows
                            If row("CheckSumACT") <> row("Checksum") Then
                                Dim Codeentier = "_" & row("Code_Badge").ToString
                                conn.Execute("Update Badges set Last_Value ='" & row("Checksum") & "'" & "where Badge_ACT ='" & Codeentier.ToString & "'")

                            End If
                        Next

                        'Form1.BADGES_SNAM.BalloonTipTitle = "IMPORT DES BADGES"
                        'Form1.BADGES_SNAM.BalloonTipText = "Import des badges reussis"
                        'Form1.BADGES_SNAM.ShowBalloonTip(1000)

                    End If

                End If
            End If
        Catch ex As Exception
            'Form1.BADGES_SNAM.BalloonTipTitle = "IMPORT DES BADGES"
            'Form1.BADGES_SNAM.BalloonTipText = "Erreur lors de l'import"
            'Form1.BADGES_SNAM.ShowBalloonTip(1000)
        End Try


    End Sub

    Public Sub lancement()

        Dim service = True
        Dim loadthread As New System.Threading.Thread(AddressOf dothread)
        loadthread.IsBackground = True
        loadthread.Name = "ACT_BADGE_SNAM"
        loadthread.Priority = Threading.ThreadPriority.Highest

        loadthread.Start()


    End Sub
    Public Sub dothread()
        Dim boucle
        connect_to_database_ACT()

        RecuperationParametres()

        connect_to_database_SNAM()
        While Threading.Thread.CurrentThread.IsAlive
            boucle = True
            While boucle = True
                updateBadge()
                System.Threading.Thread.Sleep(1000)
            End While
        End While
    End Sub


End Module
