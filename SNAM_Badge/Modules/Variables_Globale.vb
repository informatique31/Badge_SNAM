﻿Imports System.Environment
Imports MadMilkman.Ini
Module Variables_Globale
    Friend ParamsBorne As New Dictionary(Of String, String)
    Friend ParamsSqlSnam As New Dictionary(Of String, String)
    Friend ParamsFichierValeur As New Dictionary(Of String, String)
    Friend ParamsFichierNom As New Dictionary(Of String, String)

    Friend DossierConfig = GetFolderPath(SpecialFolder.CommonApplicationData) & "\" & My.Application.Info.CompanyName & "\" & My.Application.Info.ProductName
    Friend conn As ACT.Datas.SQLite
    Friend MonLog As ACT.LOG.Message
    Friend conn_SNAM As ACT.Datas.SQLServer
    Friend MonCrypt As New ACT.Crypt.AES("68fghjklB1223dfr")

    Friend Sub RecuperationParametres()
        ParamsBorne = conn.ParamsToCollection("Parametres", "BORNE")
        ParamsFichierValeur = conn.ParamsToCollection("Parametres", "FICHIERS_VALUES")
        ParamsFichierNom = conn.ParamsToCollection("Parametres", "FICHIERS_NAME")
        ParamsSqlSnam = conn.ParamsToCollection("Parametres", "SQL_SNAM")
    End Sub

    Friend Sub createdatabase_act(databasename As String)
        connect_to_database_ACT()
        Dim SqlStringCollection As New Collection
        'conn.Execute("CREATE DATABASE " & databasename & " ;", Nothing, False)
        conn.CloseConnection()
        connect_to_database_ACT()

        SqlStringCollection.Add("CREATE TABLE Parametres (
                                            Type    STRING (20)  NOT NULL,
                                            Libelle STRING (50)  NOT NULL,
                                            Valeur  STRING (255) ,
                                            unique (Type,Libelle)
                                        );")
        SqlStringCollection.Add("CREATE TABLE [Badges]([ID_act] integer NOT NULL PRIMARY KEY AUTOINCREMENT,[SNAM_BigInt] [bigint] NOT NULL,[SNAM_Hex] STRING(50) NULL,[Badge_ACT] STRING(50) NULL,[Last_value] STRING(250))")

        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('BORNE', 'ExportFolder', '')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_NAME', 'fichier0', 'Fichier_01.txt')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_NAME', 'fichier1', 'Fichier_02.txt')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_NAME', 'fichier2', 'Fichier_03.txt')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_NAME', 'fichier3', 'Fichier_04.txt')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_NAME', 'fichier4', 'Fichier_05.txt')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_NAME', 'fichier5', 'Fichier_06.txt')")

        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_VALUES', 'Fichier0', '')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_VALUES', 'Fichier1', 'nom')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_VALUES', 'Fichier2', 'prenom')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_VALUES', 'Fichier3', 'matricule')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_VALUES', 'Fichier4', 'raison_sociale')")
        SqlStringCollection.Add("INSERT INTO Parametres (Type, Libelle, Valeur) VALUES ('FICHIERS_VALUES', 'Fichier5', '')")

        SqlStringCollection.Add("INSERT  INTO Parametres (Type, Libelle, Valeur) VALUES ('SQL_SNAM', 'Catalog', '')")
        SqlStringCollection.Add("INSERT  INTO Parametres (Type, Libelle, Valeur) VALUES ('SQL_SNAM', 'Pwd', '')")
        SqlStringCollection.Add("INSERT  INTO Parametres (Type, Libelle, Valeur) VALUES ('SQL_SNAM', 'Serveur', '')")
        SqlStringCollection.Add("INSERT  INTO Parametres (Type, Libelle, Valeur) VALUES ('SQL_SNAM', 'User', 'sa')")

        For Each SqlString As String In SqlStringCollection
            conn.Execute(SqlString, Nothing, False)
        Next


        Dim file As New IniFile()
        'file.Sections("ConnectionString").Keys("Firstlaunch").Value = "FALSE"
        file.Save(DossierConfig & "\config.ini")
    End Sub


End Module
