﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TB_NUM_SNAM = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TB_ID_ACT = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.BT_Sauvegarder = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TB_PWD_SNAM = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TB_USER_SNAM = New System.Windows.Forms.TextBox()
        Me.TB_Server_SNAM = New System.Windows.Forms.TextBox()
        Me.TB_Catalog_SNAM = New System.Windows.Forms.TextBox()
        Me.TB_Chemin_Borne = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_Fichier5_VALUE = New System.Windows.Forms.TextBox()
        Me.TB_Fichier4_VALUE = New System.Windows.Forms.TextBox()
        Me.TB_Fichier5_NOM = New System.Windows.Forms.TextBox()
        Me.TB_Fichier4_NOM = New System.Windows.Forms.TextBox()
        Me.TB_Fichier3_VALUE = New System.Windows.Forms.TextBox()
        Me.TB_Fichier2_VALUE = New System.Windows.Forms.TextBox()
        Me.TB_Fichier1_VALUE = New System.Windows.Forms.TextBox()
        Me.TB_Fichier0_VALUE = New System.Windows.Forms.TextBox()
        Me.TB_Fichier3_NOM = New System.Windows.Forms.TextBox()
        Me.TB_Fichier2_NOM = New System.Windows.Forms.TextBox()
        Me.TB_Fichier1_NOM = New System.Windows.Forms.TextBox()
        Me.TB_Fichier0_NOM = New System.Windows.Forms.TextBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.DataGridViewTEST = New System.Windows.Forms.DataGridView()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SupprimerLeBadgeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BADGES_SNAM = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.AFFICHER_INTERFACE = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AfficherInterfaceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FermerLapplicationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcT_Badge_SNAM1 = New SNAM_Badge.ACT_BADGES_SNAM()
        Me.ProjectInstaller1 = New SNAM_Badge.ProjectInstaller()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.DataGridViewTEST, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.AFFICHER_INTERFACE.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(197, 384)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(163, 42)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Mise a jour badges"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(0, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(548, 341)
        Me.DataGridView1.TabIndex = 13
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(0, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(556, 370)
        Me.TabControl1.TabIndex = 14
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(548, 344)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Liste Badges ACT"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TB_NUM_SNAM)
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.TB_ID_ACT)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(548, 344)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Création Badge"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TB_NUM_SNAM
        '
        Me.TB_NUM_SNAM.Location = New System.Drawing.Point(216, 100)
        Me.TB_NUM_SNAM.Name = "TB_NUM_SNAM"
        Me.TB_NUM_SNAM.Size = New System.Drawing.Size(121, 20)
        Me.TB_NUM_SNAM.TabIndex = 6
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(216, 180)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(110, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Insérer Badge"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(99, 107)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "ID Badge SNAM"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(99, 139)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Numéro Badge ACT"
        '
        'TB_ID_ACT
        '
        Me.TB_ID_ACT.Location = New System.Drawing.Point(216, 132)
        Me.TB_ID_ACT.Name = "TB_ID_ACT"
        Me.TB_ID_ACT.Size = New System.Drawing.Size(121, 20)
        Me.TB_ID_ACT.TabIndex = 1
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.BT_Sauvegarder)
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Controls.Add(Me.TB_Chemin_Borne)
        Me.TabPage3.Controls.Add(Me.Label11)
        Me.TabPage3.Controls.Add(Me.Label10)
        Me.TabPage3.Controls.Add(Me.Label9)
        Me.TabPage3.Controls.Add(Me.Label8)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.Label6)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.TB_Fichier5_VALUE)
        Me.TabPage3.Controls.Add(Me.TB_Fichier4_VALUE)
        Me.TabPage3.Controls.Add(Me.TB_Fichier5_NOM)
        Me.TabPage3.Controls.Add(Me.TB_Fichier4_NOM)
        Me.TabPage3.Controls.Add(Me.TB_Fichier3_VALUE)
        Me.TabPage3.Controls.Add(Me.TB_Fichier2_VALUE)
        Me.TabPage3.Controls.Add(Me.TB_Fichier1_VALUE)
        Me.TabPage3.Controls.Add(Me.TB_Fichier0_VALUE)
        Me.TabPage3.Controls.Add(Me.TB_Fichier3_NOM)
        Me.TabPage3.Controls.Add(Me.TB_Fichier2_NOM)
        Me.TabPage3.Controls.Add(Me.TB_Fichier1_NOM)
        Me.TabPage3.Controls.Add(Me.TB_Fichier0_NOM)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(548, 344)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Paramétres"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'BT_Sauvegarder
        '
        Me.BT_Sauvegarder.Location = New System.Drawing.Point(193, 301)
        Me.BT_Sauvegarder.Name = "BT_Sauvegarder"
        Me.BT_Sauvegarder.Size = New System.Drawing.Size(163, 40)
        Me.BT_Sauvegarder.TabIndex = 23
        Me.BT_Sauvegarder.Text = "Sauvegarder"
        Me.BT_Sauvegarder.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.TB_PWD_SNAM)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.TB_USER_SNAM)
        Me.GroupBox1.Controls.Add(Me.TB_Server_SNAM)
        Me.GroupBox1.Controls.Add(Me.TB_Catalog_SNAM)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 220)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(538, 75)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Connexion a la base"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(281, 52)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(33, 13)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "PWD"
        '
        'TB_PWD_SNAM
        '
        Me.TB_PWD_SNAM.Location = New System.Drawing.Point(320, 45)
        Me.TB_PWD_SNAM.Name = "TB_PWD_SNAM"
        Me.TB_PWD_SNAM.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TB_PWD_SNAM.Size = New System.Drawing.Size(113, 20)
        Me.TB_PWD_SNAM.TabIndex = 28
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(108, 52)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(29, 13)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "User"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(281, 26)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(38, 13)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "Server"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(108, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(43, 13)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "Catalog"
        '
        'TB_USER_SNAM
        '
        Me.TB_USER_SNAM.Location = New System.Drawing.Point(157, 45)
        Me.TB_USER_SNAM.Name = "TB_USER_SNAM"
        Me.TB_USER_SNAM.Size = New System.Drawing.Size(113, 20)
        Me.TB_USER_SNAM.TabIndex = 25
        '
        'TB_Server_SNAM
        '
        Me.TB_Server_SNAM.Location = New System.Drawing.Point(320, 19)
        Me.TB_Server_SNAM.Name = "TB_Server_SNAM"
        Me.TB_Server_SNAM.Size = New System.Drawing.Size(113, 20)
        Me.TB_Server_SNAM.TabIndex = 24
        '
        'TB_Catalog_SNAM
        '
        Me.TB_Catalog_SNAM.Location = New System.Drawing.Point(157, 19)
        Me.TB_Catalog_SNAM.Name = "TB_Catalog_SNAM"
        Me.TB_Catalog_SNAM.Size = New System.Drawing.Size(113, 20)
        Me.TB_Catalog_SNAM.TabIndex = 23
        '
        'TB_Chemin_Borne
        '
        Me.TB_Chemin_Borne.Location = New System.Drawing.Point(223, 194)
        Me.TB_Chemin_Borne.Name = "TB_Chemin_Borne"
        Me.TB_Chemin_Borne.Size = New System.Drawing.Size(153, 20)
        Me.TB_Chemin_Borne.TabIndex = 21
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(118, 197)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 13)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Chemin de la Borne"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(320, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(36, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "valeur"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(211, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(27, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "nom"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(118, 165)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "fichier5"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(118, 138)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "fichier4"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(118, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "fichier3"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(118, 85)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "fichier2"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(118, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "fichier 1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(118, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "fichier 0"
        '
        'TB_Fichier5_VALUE
        '
        Me.TB_Fichier5_VALUE.Location = New System.Drawing.Point(299, 158)
        Me.TB_Fichier5_VALUE.Name = "TB_Fichier5_VALUE"
        Me.TB_Fichier5_VALUE.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier5_VALUE.TabIndex = 11
        '
        'TB_Fichier4_VALUE
        '
        Me.TB_Fichier4_VALUE.Location = New System.Drawing.Point(299, 131)
        Me.TB_Fichier4_VALUE.Name = "TB_Fichier4_VALUE"
        Me.TB_Fichier4_VALUE.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier4_VALUE.TabIndex = 10
        '
        'TB_Fichier5_NOM
        '
        Me.TB_Fichier5_NOM.Location = New System.Drawing.Point(193, 158)
        Me.TB_Fichier5_NOM.Name = "TB_Fichier5_NOM"
        Me.TB_Fichier5_NOM.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier5_NOM.TabIndex = 9
        '
        'TB_Fichier4_NOM
        '
        Me.TB_Fichier4_NOM.Location = New System.Drawing.Point(193, 131)
        Me.TB_Fichier4_NOM.Name = "TB_Fichier4_NOM"
        Me.TB_Fichier4_NOM.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier4_NOM.TabIndex = 8
        '
        'TB_Fichier3_VALUE
        '
        Me.TB_Fichier3_VALUE.Location = New System.Drawing.Point(299, 105)
        Me.TB_Fichier3_VALUE.Name = "TB_Fichier3_VALUE"
        Me.TB_Fichier3_VALUE.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier3_VALUE.TabIndex = 7
        '
        'TB_Fichier2_VALUE
        '
        Me.TB_Fichier2_VALUE.Location = New System.Drawing.Point(299, 78)
        Me.TB_Fichier2_VALUE.Name = "TB_Fichier2_VALUE"
        Me.TB_Fichier2_VALUE.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier2_VALUE.TabIndex = 6
        '
        'TB_Fichier1_VALUE
        '
        Me.TB_Fichier1_VALUE.Location = New System.Drawing.Point(299, 51)
        Me.TB_Fichier1_VALUE.Name = "TB_Fichier1_VALUE"
        Me.TB_Fichier1_VALUE.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier1_VALUE.TabIndex = 5
        '
        'TB_Fichier0_VALUE
        '
        Me.TB_Fichier0_VALUE.Location = New System.Drawing.Point(299, 24)
        Me.TB_Fichier0_VALUE.Name = "TB_Fichier0_VALUE"
        Me.TB_Fichier0_VALUE.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier0_VALUE.TabIndex = 4
        '
        'TB_Fichier3_NOM
        '
        Me.TB_Fichier3_NOM.Location = New System.Drawing.Point(193, 105)
        Me.TB_Fichier3_NOM.Name = "TB_Fichier3_NOM"
        Me.TB_Fichier3_NOM.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier3_NOM.TabIndex = 3
        '
        'TB_Fichier2_NOM
        '
        Me.TB_Fichier2_NOM.Location = New System.Drawing.Point(193, 78)
        Me.TB_Fichier2_NOM.Name = "TB_Fichier2_NOM"
        Me.TB_Fichier2_NOM.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier2_NOM.TabIndex = 2
        '
        'TB_Fichier1_NOM
        '
        Me.TB_Fichier1_NOM.Location = New System.Drawing.Point(193, 51)
        Me.TB_Fichier1_NOM.Name = "TB_Fichier1_NOM"
        Me.TB_Fichier1_NOM.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier1_NOM.TabIndex = 1
        '
        'TB_Fichier0_NOM
        '
        Me.TB_Fichier0_NOM.Location = New System.Drawing.Point(193, 24)
        Me.TB_Fichier0_NOM.Name = "TB_Fichier0_NOM"
        Me.TB_Fichier0_NOM.Size = New System.Drawing.Size(77, 20)
        Me.TB_Fichier0_NOM.TabIndex = 0
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.DataGridViewTEST)
        Me.TabPage4.Controls.Add(Me.Button3)
        Me.TabPage4.Controls.Add(Me.RichTextBox1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(548, 344)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "SQL"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'DataGridViewTEST
        '
        Me.DataGridViewTEST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewTEST.Location = New System.Drawing.Point(56, 174)
        Me.DataGridViewTEST.Name = "DataGridViewTEST"
        Me.DataGridViewTEST.Size = New System.Drawing.Size(437, 121)
        Me.DataGridViewTEST.TabIndex = 3
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(232, 301)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(56, 53)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(437, 96)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SupprimerLeBadgeToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(178, 26)
        '
        'SupprimerLeBadgeToolStripMenuItem
        '
        Me.SupprimerLeBadgeToolStripMenuItem.Name = "SupprimerLeBadgeToolStripMenuItem"
        Me.SupprimerLeBadgeToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.SupprimerLeBadgeToolStripMenuItem.Text = "Supprimer le Badge"
        '
        'BADGES_SNAM
        '
        Me.BADGES_SNAM.ContextMenuStrip = Me.AFFICHER_INTERFACE
        Me.BADGES_SNAM.Icon = CType(resources.GetObject("BADGES_SNAM.Icon"), System.Drawing.Icon)
        Me.BADGES_SNAM.Text = "Badges_SNAM"
        '
        'AFFICHER_INTERFACE
        '
        Me.AFFICHER_INTERFACE.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AfficherInterfaceToolStripMenuItem, Me.FermerLapplicationToolStripMenuItem})
        Me.AFFICHER_INTERFACE.Name = "AFFICHER_INTERFACE"
        Me.AFFICHER_INTERFACE.Size = New System.Drawing.Size(217, 48)
        '
        'AfficherInterfaceToolStripMenuItem
        '
        Me.AfficherInterfaceToolStripMenuItem.Name = "AfficherInterfaceToolStripMenuItem"
        Me.AfficherInterfaceToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.AfficherInterfaceToolStripMenuItem.Text = "Afficher/Masquer interface"
        '
        'FermerLapplicationToolStripMenuItem
        '
        Me.FermerLapplicationToolStripMenuItem.Name = "FermerLapplicationToolStripMenuItem"
        Me.FermerLapplicationToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.FermerLapplicationToolStripMenuItem.Text = "Fermer l'application"
        '
        'AcT_Badge_SNAM1
        '
        Me.AcT_Badge_SNAM1.ExitCode = 0
        Me.AcT_Badge_SNAM1.ServiceName = "ACT_Badge_SNAM"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(552, 421)
        Me.ControlBox = False
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.ShowInTaskbar = False
        Me.Text = "SNAM_Badge"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        CType(Me.DataGridViewTEST, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.AFFICHER_INTERFACE.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As Button
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents TB_ID_ACT As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TB_Fichier5_VALUE As TextBox
    Friend WithEvents TB_Fichier4_VALUE As TextBox
    Friend WithEvents TB_Fichier5_NOM As TextBox
    Friend WithEvents TB_Fichier4_NOM As TextBox
    Friend WithEvents TB_Fichier3_VALUE As TextBox
    Friend WithEvents TB_Fichier2_VALUE As TextBox
    Friend WithEvents TB_Fichier1_VALUE As TextBox
    Friend WithEvents TB_Fichier0_VALUE As TextBox
    Friend WithEvents TB_Fichier3_NOM As TextBox
    Friend WithEvents TB_Fichier2_NOM As TextBox
    Friend WithEvents TB_Fichier1_NOM As TextBox
    Friend WithEvents TB_Fichier0_NOM As TextBox
    Friend WithEvents AcT_Badge_SNAM1 As ACT_BADGES_SNAM
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label15 As Label
    Friend WithEvents TB_PWD_SNAM As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents TB_USER_SNAM As TextBox
    Friend WithEvents TB_Server_SNAM As TextBox
    Friend WithEvents TB_Catalog_SNAM As TextBox
    Friend WithEvents TB_Chemin_Borne As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents BT_Sauvegarder As Button
    Friend WithEvents TB_NUM_SNAM As TextBox
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents SupprimerLeBadgeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents Button3 As Button
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents ProjectInstaller1 As ProjectInstaller
    Friend WithEvents DataGridViewTEST As DataGridView
    Friend WithEvents BADGES_SNAM As NotifyIcon
    Friend WithEvents AFFICHER_INTERFACE As ContextMenuStrip
    Friend WithEvents AfficherInterfaceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FermerLapplicationToolStripMenuItem As ToolStripMenuItem
End Class
