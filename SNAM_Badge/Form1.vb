﻿Imports MadMilkman.Ini
Imports System.Numerics
Imports System.Environment
Public Class Form1
    Public Sub New()

        ' Cet appel est requis par le concepteur.
        InitializeComponent()

        ' Ajoutez une initialisation quelconque après l'appel InitializeComponent().

    End Sub

    Private Sub Form1_Loadini(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Visible = False
        ' Create new file with a default formatting.
        Dim file As New IniFile()
        'Dim DossierConfig = GetFolderPath(SpecialFolder.CommonApplicationData) & "\ACT\Serveur_pesee_Cyber_STAT"

        'The file exists 
        If System.IO.File.Exists(DossierConfig & "\config.ini") Then
            file.Load(DossierConfig & "\config.ini")

        Else
            'MsgBox(DossierConfig)

            My.Computer.FileSystem.CreateDirectory(DossierConfig)
            MsgBox("Pas de fichier de config")

            Dim Result As Boolean
            Dim Base As String = ""
            'Dim catalog As String = ""
            'Dim user As String = ""

            Dim pwd = ""

            Do
                Base = InputBox("Instance", "Chemin de la base SQLITE")
                If Base = "" Then
                    Result = False
                Else
                    Result = True
                End If
            Loop While (Not Result)


            Dim section1 As IniSection = file.Sections.Add("ConnectionString")

            Dim instanceIni As IniKey = section1.Keys.Add("instance", Base)

            Dim firstlaunch = section1.Keys.Add("Firstlaunch", "TRUE")
            file.Save(DossierConfig & "\config.ini")
            MsgBox("Config sauvegarder")
        End If

        If file.Sections("ConnectionString").Keys("Firstlaunch").Value = "TRUE" Then
            createdatabase_act("Badge_ACT")
            MsgBox("BDD ACT creer")
            file.Sections("ConnectionString").Keys("Firstlaunch").Value = "FALSE"
        End If
        file.Save(DossierConfig & "\config.ini")
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        BADGES_SNAM.BalloonTipTitle = "SNAM_BADGE TEST IMPORT "
        BADGES_SNAM.BalloonTipText = "Test de l'import des badges dans la borne"
        BADGES_SNAM.ShowBalloonTip(1000)
        updateBadge(True)
    End Sub

    Private Sub load_form(sender As Object, e As EventArgs) Handles Me.Load
        Me.Visible = False
        Me.Hide()
        Me.BADGES_SNAM.Visible = True


        Try
            'cache la page admin sql'
            TabControl1.TabPages.Remove(TabPage4)
            TabControl1.TabPages.Remove(TabPage3)
            connect_to_database_ACT()

            RecuperationParametres()


            TB_Chemin_Borne.Text = ParamsBorne("ExportFolder")
            TB_Fichier0_NOM.Text = ParamsFichierNom("fichier0")
            TB_Fichier1_NOM.Text = ParamsFichierNom("fichier1")
            TB_Fichier2_NOM.Text = ParamsFichierNom("fichier2")
            TB_Fichier3_NOM.Text = ParamsFichierNom("fichier3")
            TB_Fichier4_NOM.Text = ParamsFichierNom("fichier4")
            TB_Fichier5_NOM.Text = ParamsFichierNom("fichier5")

            TB_Fichier0_VALUE.Text = ParamsFichierValeur("Fichier0")
            TB_Fichier1_VALUE.Text = ParamsFichierValeur("Fichier1")
            TB_Fichier2_VALUE.Text = ParamsFichierValeur("Fichier2")
            TB_Fichier3_VALUE.Text = ParamsFichierValeur("Fichier3")
            TB_Fichier4_VALUE.Text = ParamsFichierValeur("Fichier4")
            TB_Fichier5_VALUE.Text = ParamsFichierValeur("Fichier5")

            If ParamsSqlSnam("Pwd") <> Nothing And ParamsSqlSnam("Pwd") <> "" Then
                TB_PWD_SNAM.Text = MonCrypt.DechiffrerChaine(ParamsSqlSnam("Pwd"))
            Else
                TB_PWD_SNAM.Text = ""
            End If
            TB_Server_SNAM.Text = ParamsSqlSnam("Serveur")
            TB_USER_SNAM.Text = ParamsSqlSnam("User")
            TB_Catalog_SNAM.Text = ParamsSqlSnam("Catalog")
            '  If ParamsSqlSnam("Catalog") <> Nothing And ParamsSqlSnam("Catalog") <> "" And ParamsSqlSnam("Serveur") <> Nothing And ParamsSqlSnam("Serveur") <> "" And ParamsSqlSnam("Pwd") <> Nothing And ParamsSqlSnam("Pwd") <> "" Then
            connect_to_database_SNAM()
            '      updateCB_SNAM()
            '  End If

            Dim MonDs As New DataSet

            'conn.AddSqlCommandToDataSet(MonDs, "BadgeAct", "select * from badges")

            Dim mesbadges As New List(Of String)
            Dim dsbadge As New DataSet
            conn.AddSqlCommandToDataSet(dsbadge, "liste_badge", "Select Snam_BigInt,replace(CAST(Badge_ACT AS TEXT),'_','')As Badge_ACT ,Snam_Hex FROM Badges")
            Me.DataGridView1.DataSource = dsbadge.Tables(0)
            sender.visible = False


            lancement()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        notvisible()
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub Save_Badge(sender As Object, e As EventArgs) Handles Button2.Click

        'Sauvegarde les badge dans la BDD ACT 


        If TB_ID_ACT.Text <> "" And TB_NUM_SNAM.Text.Trim <> "" Then

            Dim verifNUM = conn.GetRowCount("Select * from badges where SNAM_Hex ='" & TB_NUM_SNAM.Text & "'")
            Dim VerifSNAM = conn.GetRowCount("Select * from badges where BADGE_ACT ='_" & TB_ID_ACT.Text & "'")
            Dim Code_ACT_prefixe = "_" & TB_ID_ACT.Text
            'MsgBox(Hex(TB_NUM_SNAM.Text))
            If verifNUM > 0 And VerifSNAM <= 0 Then
                conn.Execute("Update Badges Set BADGE_ACT ='" & Code_ACT_prefixe & "'where SNAM_Hex ='" & TB_NUM_SNAM.Text & "'")
                MsgBox("Badge Mis a jour")
            End If
            If VerifSNAM > 0 And verifNUM <= 0 Then
                conn.Execute("Update Badges Set SNAM_Hex ='" & TB_NUM_SNAM.Text & "', SNAM_BigInt ='" & CStr(CLng("&H" & TB_NUM_SNAM.Text)) & "'where BADGE_ACT ='" & Code_ACT_prefixe & "'")
                MsgBox("Badge Mis a jour")
            End If

            If verifNUM <= 0 And VerifSNAM <= 0 Then
                Try

                    '        conn.Execute("Insert into Badges(SNAM_Hex,SNAM_Bigint,BADGE_ACT,Last_value) Values('" & TB_NUM_SNAM.Text & "','" & CStr(CLng("&H" & TB_NUM_SNAM.Text)) & "' ,'" & Code_ACT_prefixe & "','-1')")



                    '        MsgBox("Nouveau Badge insérer dans la base")

                    MsgBox("Veuillez taper un numero de Badge ACT existant dans la liste")

                Catch ex As Exception
                    MsgBox(ex)
                End Try

            End If
            '  updateCB_SNAM()
            update_DGV_ACT()
            End If
    End Sub

    Private Sub MDIParent_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TabControl1.KeyDown
        If e.Alt And e.Control Then

            Dim Password As String

            'Password = ACT.FORMS.Form.MessageBoxInput("Administration", "Merci de saisir le mot de passe.")
            Password = ACT.FORMS.Form.InputPassword("Merci de saisir le mot de passe.", "Administration")

            If Password = "act12rodez" Then
                TabControl1.TabPages.Add(TabPage4)
                TabControl1.TabPages.Add(TabPage3)
            End If

        End If
    End Sub

    'Private Sub updateCB_SNAM()

    '    Dim MonDs As New DataSet
    '    Dim mesbadges_Existants As New List(Of String)
    '    conn.AddSqlCommandToDataSet(MonDs, "BadgeAct", "select * from badges")

    '    For Each row In MonDs.Tables("BadgeAct").Rows
    '        mesbadges_Existants.Add(row("SNAM_BigInt"))
    '    Next

    '    If mesbadges_Existants.Count <> 0 Then
    '        'Remplis la combobox avec l'id des badges qui n'existent pas '
    '        conn_SNAM.InitCombo(CB_ID_SNAM, "Select numero,numero as num from acbadge where numero not in (" & Join(mesbadges_Existants.ToArray, ",") & ")")
    '    End If
    'End Sub


    Private Sub update_DGV_ACT()
        Try

            Dim dsbadge As New DataSet
            conn.AddSqlCommandToDataSet(dsbadge, "liste_badge", "Select Snam_BigInt,replace(CAST(Badge_ACT AS TEXT),'_','')As Badge_ACT ,Snam_Hex FROM Badges")
            Me.DataGridView1.DataSource = dsbadge.Tables(0)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BT_Sauvegarder_Click(sender As Object, e As EventArgs) Handles BT_Sauvegarder.Click
        Try

            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier0_NOM.Text & "'where libelle = 'fichier0' and Type = 'FICHIERS_NAME'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier1_NOM.Text & "'where libelle = 'fichier1' and Type = 'FICHIERS_NAME'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier2_NOM.Text & "'where libelle = 'fichier2' and Type = 'FICHIERS_NAME'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier3_NOM.Text & "'where libelle = 'fichier3' and Type = 'FICHIERS_NAME'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier4_NOM.Text & "'where libelle = 'fichier4' and Type = 'FICHIERS_NAME'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier5_NOM.Text & "'where libelle = 'fichier5' and Type = 'FICHIERS_NAME'")

            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier0_VALUE.Text & "'where libelle = 'Fichier0' and Type = 'FICHIERS_VALUES'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier1_VALUE.Text & "'where libelle = 'Fichier1' and Type = 'FICHIERS_VALUES'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier2_VALUE.Text & "'where libelle = 'Fichier2' and Type = 'FICHIERS_VALUES'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier3_VALUE.Text & "'where libelle = 'Fichier3' and Type = 'FICHIERS_VALUES'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier4_VALUE.Text & "'where libelle = 'Fichier4' and Type = 'FICHIERS_VALUES'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Fichier5_VALUE.Text & "'where libelle = 'Fichier5' and Type = 'FICHIERS_VALUES'")

            conn.Execute("Update Parametres Set  Valeur = '" & TB_Chemin_Borne.Text & "'where libelle = 'ExportFolder'")

            conn.Execute("Update Parametres Set  Valeur = '" & TB_Catalog_SNAM.Text & "'where libelle = 'Catalog'")
            conn.Execute("Update Parametres Set  Valeur = '" & MonCrypt.ChiffrerChaine(TB_PWD_SNAM.Text) & "'where libelle = 'Pwd'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_Server_SNAM.Text & "'where libelle = 'Serveur'")
            conn.Execute("Update Parametres Set  Valeur = '" & TB_USER_SNAM.Text & "'where libelle = 'User'")

            MsgBox("Paramétres Bien sauvegarder")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    'Private Sub DataGridView1_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseClick
    '    ContextMenuStrip1.Show(DataGridView1, New Point(e.X, e.Y))

    'End Sub

    'Private Sub SupprimerLeBadgeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SupprimerLeBadgeToolStripMenuItem.Click
    '    Dim row = DataGridView1.SelectedCells(0).Value
    '    conn.Execute("Delete From Badges where SNAM_BIGINT = '" & row & "'")

    '    update_DGV_ACT()
    'End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim dstest As New DataSet
        conn_SNAM.AddSqlCommandToDataSet(dstest, "test", RichTextBox1.Text)
        Me.DataGridViewTEST.DataSource = dstest.Tables(0)
    End Sub

    Private Sub AfficherInterfaceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AfficherInterfaceToolStripMenuItem.Click
        If Me.Visible = True Then
            Me.Hide()
        Else
            Me.Show()

        End If
    End Sub

    Private Sub FermerLapplicationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FermerLapplicationToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub notvisible()

        If Me.Visible = True Then
            Me.Visible = False
            Me.Hide()
        End If
    End Sub
    Private Sub Form1_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Me.Visible = False
        'NotifyIcon1.Visible = True
    End Sub


End Class
