﻿@echo off
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "%~dp0\..\SNAM_Badge.exe" 

IF ERRORLEVEL 1 goto erreur

sc config ACT_Badge_SNAM binpath="%~dp0\..\SNAM_Badge.exe -Service"
sc start ACT_Badge_SNAM

IF ERRORLEVEL 1 goto Erreur

goto Fin

:Erreur

pause

:Fin